//
//  ViewController.swift
//  MapsTesting
//
//  Created by Family on 04/09/19.
//  Copyright © 2019 Family. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import MapKit

var resultViewController : GMSAutocompleteResultsViewController?
var searchController : UISearchController?

class ViewController: UIViewController {
    @IBOutlet weak var mapKit: MKMapView!
    
    
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        setupSearchController()
    }
    
    func setupSearchController() {
        resultViewController = GMSAutocompleteResultsViewController()
        searchController = UISearchController(searchResultsController: resultViewController)
        searchController?.searchResultsUpdater = resultViewController
        
        let searchBar = searchController!.searchBar
        searchBar.sizeToFit()
        searchBar.placeholder = "search sometin in here"
        navigationItem.titleView = searchController?.searchBar// ini buat naro searchbar di navigation item
        definesPresentationContext = true
        searchController?.hidesNavigationBarDuringPresentation = false
        
        
        resultViewController?.delegate = self
    }
}

extension ViewController: GMSAutocompleteResultsViewControllerDelegate{
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didAutocompleteWith place: GMSPlace) {
        //1
        searchController?.isActive = false
        
        //2
        mapKit.removeAnnotation(mapKit?.annotations as! MKAnnotation)
        
        //3
        let span = MKCoordinateSpan(latitudeDelta: 0.05, longitudeDelta: 0.05)
        let region = MKCoordinateRegion(center: place.coordinate, span: span)
        mapKit.setRegion(region, animated: true)
        
        //4
        let annotation = MKPointAnnotation()
        annotation.coordinate = place.coordinate
        annotation.title = place.name
        annotation.subtitle = place.formattedAddress
        mapKit.addAnnotation(annotation)
        
        
    }
    
    func resultsController(_ resultsController: GMSAutocompleteResultsViewController, didFailAutocompleteWithError error: Error) {
        print("error nich 😝\(error.localizedDescription)")
    }
    
    
    
}

